﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class AspectRelationsHelper
{
    public class AspectRelations
    {
        public Aspect Aspect;
        public List<Aspect> Related;
        public List<Aspect> Neutral;
        public List<Aspect> Unrelated;
        public Aspect Opposite;
    }

    public static AspectRelations GetAspectRelations(Aspect aspect)
    {
        return new AspectRelations()
        {
            Aspect = aspect,
            Related = GetRelatedAspects(aspect),
            Neutral = GetNeutralAspects(aspect),
            Unrelated = GetUnrelatedAspects(aspect),
            Opposite = GetOppositeAspect(aspect)
        };
    }

    public static List<Aspect> GetRelatedAspects(Aspect aspect)
    {
        return GetAspectRelationByDistance(aspect, 1);
    }

    public static List<Aspect> GetNeutralAspects(Aspect aspect)
    {
        return GetAspectRelationByDistance(aspect, 2);
    }

    public static List<Aspect> GetUnrelatedAspects(Aspect aspect)
    {
        return GetAspectRelationByDistance(aspect, 3);
    }

    public static Aspect GetOppositeAspect(Aspect aspect)
    {
        return GetAspectRelationByDistance(aspect, 4).First();
    }

    public static List<Aspect> GetAspectRelationByDistance(Aspect aspect, int distance)
    {
        var positiveDistance = (int)aspect + distance;

        if (positiveDistance >= 9)
        {
            positiveDistance -= 8;
        }

        var negativeDistance = (int)aspect - distance;

        if (negativeDistance <= 0)
        {
            negativeDistance += 8;
        }

        return new List<Aspect>()
        {
            (Aspect)positiveDistance,
            (Aspect)negativeDistance
        };
    }
}
