﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Helpers
{
    public static class HexHelper
    {
        #region CubeOperations
        /// <summary>
        /// Adds two cube-based coordinates.
        /// </summary>
        /// <param name="cube_1"></param>
        /// <param name="cube_2"></param>
        /// <returns></returns>
        public static Vector3 CubeAdd(Vector3 cube_1, Vector3 cube_2)
        {
            return cube_1 + cube_2;
        }

        /// <summary>
        /// Subtracts two cube-based coordinates.
        /// </summary>
        /// <param name="cube_1"></param>
        /// <param name="cube_2"></param>
        /// <returns></returns>
        public static Vector3 CubeSubtract(Vector3 cube_1, Vector3 cube_2)
        {
            return cube_1 - cube_2;
        }

        /// <summary>
        /// Multiply cube-based coordinate by value.
        /// </summary>
        /// <param name="cube_1"></param>
        /// <param name="k"></param>
        /// <returns></returns>
        public static Vector3 CubeMultiply(Vector3 cube_1, int k)
        {
            return new Vector3(
                cube_1.x * k,
                cube_1.y * k,
                cube_1.z * k
            );
        }
        #endregion

        #region AxialOperations
        /// <summary>
        /// Adds two axial-based coordinates.
        /// </summary>
        /// <param name="axial_1"></param>
        /// <param name="axial_2"></param>
        /// <returns></returns>
        public static Vector2 AxialAdd(Vector2 axial_1, Vector2 axial_2)
        {
            return CubeToAxial(CubeAdd(AxialToCube(axial_1), AxialToCube(axial_2)));
        }

        /// <summary>
        /// Subtracts two axial-based coordinates.
        /// </summary>
        /// <param name="axial_1"></param>
        /// <param name="axial_2"></param>
        /// <returns></returns>
        public static Vector2 AxialSubtract(Vector2 axial_1, Vector2 axial_2)
        {
            return CubeToAxial(CubeSubtract(AxialToCube(axial_1), AxialToCube(axial_2)));
        }

        /// <summary>
        /// Multiply axial-based coordinate by value.
        /// </summary>
        /// <param name="axial_1"></param>
        /// <param name="k"></param>
        /// <returns></returns>
        public static Vector2 AxialMultiply(Vector2 axial_1, int k)
        {
            return CubeToAxial(CubeMultiply(AxialToCube(axial_1), k));
        }
        #endregion

        #region OffsetOperations
        /// <summary>
        /// Adds two offset-based coordinates.
        /// </summary>
        /// <param name="offset_1"></param>
        /// <param name="offset_2"></param>
        /// <returns></returns>
        public static Vector2 OffsetAdd(Vector2 offset_1, Vector2 offset_2)
        {
            return CubeToOffset(CubeAdd(OffsetToCube(offset_1), OffsetToCube(offset_2)));
        }

        /// <summary>
        /// Subtracts two offset-based coordinates.
        /// </summary>
        /// <param name="offset_1"></param>
        /// <param name="offset_2"></param>
        /// <returns></returns>
        public static Vector2 OffsetSubtract(Vector2 offset_1, Vector2 offset_2)
        {
            return CubeToOffset(CubeSubtract(OffsetToCube(offset_1), OffsetToCube(offset_2)));
        }

        /// <summary>
        /// Multiply offset-based coordinate by value.
        /// </summary>
        /// <param name="offset_1"></param>
        /// <param name="k"></param>
        /// <returns></returns>
        public static Vector2 OffsetMultiply(Vector2 offset_1, int k)
        {
            return CubeToOffset(CubeMultiply(OffsetToCube(offset_1), k));
        }
        #endregion

        #region Conversions Axial <-> Cube
        /// <summary>
        /// Converts cube-based coordinates to axial-based coordinates
        /// </summary>
        /// <param name="cube"></param>
        /// <returns></returns>
        public static Vector2 CubeToAxial(Vector3 cube)
        {
            var q = cube.x;
            var r = cube.z;
            return new Vector2(q, r);
        }

        /// <summary>
        /// Converts axial-based coordinates to cube-based coordinates
        /// </summary>
        /// <param name="axial"></param>
        /// <returns></returns>
        public static Vector3 AxialToCube(Vector2 axial)
        {
            var x = axial.x;
            var z = axial.y;
            var y = -x - z;
            return new Vector3(x, y, z);
        }
        #endregion

        #region Conversions Offset <-> Cube
        /// <summary>
        /// Converts cube-based coordinates to offset-based coordinates
        /// </summary>
        /// <param name="cube"></param>
        /// <returns></returns>
        public static Vector2 CubeToOffset(Vector3 cube)
        {
            var x = cube.x + (((int)cube.z - ((int)cube.z & 1)) >> 1);
            var y = cube.z;
            return new Vector2(x, y);
        }

        /// <summary>
        /// Converts offset-based coordinates to cube-based coordinates
        /// </summary>
        /// <param name="axial"></param>
        /// <returns></returns>
        public static Vector3 OffsetToCube(Vector2 offset)
        {
            var x = offset.x - (((int)offset.y - ((int)offset.y & 1)) >> 1);
            var z = offset.y;
            var y = -x - z;
            return new Vector3(x, y, z);
        }
        #endregion

        #region CubeRound()
        /// <summary>
        /// Rounds cube-based coordinates with float values to int values
        /// </summary>
        /// <param name="cube"></param>
        /// <returns></returns>
        public static Vector3 CubeRound(Vector3 cube)
        {
            var rx = Mathf.Round(cube.x);
            var ry = Mathf.Round(cube.y);
            var rz = Mathf.Round(cube.z);

            var xDiff = Mathf.Abs(rx - cube.x);
            var yDiff = Mathf.Abs(ry - cube.y);
            var zDiff = Mathf.Abs(rz - cube.z);

            if (xDiff > yDiff && xDiff > zDiff)
            {
                rx = -ry - rz;
            }
            else if (yDiff > zDiff)
            {
                ry = -rx - rz;
            }
            else
            {
                rz = -rx - ry;
            }

            return new Vector3(rx, ry, rz);
        }
        #endregion

        #region AxialRound()
        /// <summary>
        /// Rounds axial-based coordinates with float values to int values
        /// </summary>
        /// <param name="hex"></param>
        /// <returns></returns>
        public static Vector2 AxialRound(Vector2 hex)
        {
            return CubeToAxial(CubeRound(AxialToCube(hex)));
        }
        #endregion

        #region GetRange()
        /// <summary>
        /// Returns all hexes inside range of center. Center hex position is returned as well.
        /// </summary>
        /// <param name="centerHexPosition"></param>
        /// <param name="allHexPositions"></param>
        /// <param name="range">Range of center hex</param>
        /// <returns>List of Hex objects that are in range of center hex</returns>
        public static List<Vector3> GetRangeCube(Vector3 centerHexPosition, List<Vector3> allHexPositions, int range)
        {
            var results = new List<Vector3>();

            for (var x = -range; x <= range; x++)
            {
                var y1 = Mathf.Max(-range, -x - range);
                var y2 = Mathf.Min(range, -x + range);

                for (var y = y1; y <= y2; y++)
                {
                    var z = -x - y;
                    var position = CubeAdd(centerHexPosition, new Vector3(x, y, z));

                    try
                    {
                        var hexInRange = allHexPositions.Where(h => h.x == position.x && h.y == position.y).First();

                        if (hexInRange != null)
                        {
                            results.Add(hexInRange);
                        }
                    }
                    catch (InvalidOperationException)
                    {
                        continue;
                    }
                }
            }

            return results;
        }

        /// <summary>
        /// Returns all hexes inside range of center. Center hex position is returned as well.
        /// </summary>
        /// <param name="centerHexPosition"></param>
        /// <param name="allHexPositions"></param>
        /// <param name="range">Range of center hex</param>
        /// <returns>List of Hex objects that are in range of center hex</returns>
        public static List<Vector3> GetRangeAxial(Vector3 centerHexPosition, List<Vector3> allHexPositions, int range)
        {
            var results = new List<Vector3>();
            var axialCoords = new Vector2();

            for (var q = -range; q <= range; q++)
            {
                var r1 = Mathf.Max(-range, -q - range);
                var r2 = Mathf.Min(range, -q + range);

                for (var r = r1; r <= r2; r++)
                {
                    axialCoords.x = r;
                    axialCoords.y = q;

                    axialCoords = AxialAdd(centerHexPosition, axialCoords);

                    try
                    {
                        var hexInRange = allHexPositions.Where(h => h.x == axialCoords.x && h.y == axialCoords.y).First();

                        if (hexInRange != null)
                        {
                            results.Add(hexInRange);
                        }
                    }
                    catch (InvalidOperationException)
                    {
                        continue;
                    }
                }
            }

            return results;
        }

        /// <summary>
        /// Returns all hexes inside range of center. Center hex position is returned as well.
        /// </summary>
        /// <param name="centerHexPosition"></param>
        /// <param name="allHexPositions"></param>
        /// <param name="range">Range of center hex</param>
        /// <returns>List of Hex objects that are in range of center hex</returns>
        public static List<Vector3> GetRangeOffset(Vector3 centerHexPosition, List<Vector3> allHexPositions, int range)
        {
            var results = new List<Vector3>();

            for (var x = -range; x <= range; x++)
            {
                var y1 = Mathf.Max(-range, -x - range);
                var y2 = Mathf.Min(range, -x + range);

                for (var y = y1; y <= y2; y++)
                {
                    var z = -x - y;
                    var position = CubeAdd(OffsetToCube(centerHexPosition), new Vector3(x, y, z));
                    position = CubeToOffset(position);

                    try
                    {
                        var hexInRange = allHexPositions.Where(h => h.x == position.x && h.y == position.y).First();

                        if (hexInRange != null)
                        {
                            results.Add(hexInRange);
                        }
                    }
                    catch (InvalidOperationException)
                    {
                        continue;
                    }
                }
            }

            return results;
        }
        #endregion

        public static List<Vector3> GetLinesCube(Vector3 centerHexPosition, List<Vector3> allHexPositions, int range)
        {
            var rangePositions = GetRangeCube(centerHexPosition, allHexPositions, range);
            return rangePositions.Where(pos => pos.x == centerHexPosition.x || pos.y == centerHexPosition.y || pos.z == centerHexPosition.z).ToList();
        }

        public static List<Vector3> GetLinesOffset(Vector3 centerHexPosition, List<Vector3> allHexPositions, int range)
        {
            var cubePositions = allHexPositions.Select(x => OffsetToCube(x)).ToList();
            var cubeCenter = OffsetToCube(centerHexPosition);

            return GetLinesCube(cubeCenter, cubePositions, range).Select(x => (Vector3)CubeToOffset(x)).ToList();
        }

        public static bool IsInRangeOffset(Vector3 currentPosition, Vector3 destinationPosition, int range)
        {
            for (var x = -range; x <= range; x++)
            {
                var y1 = Mathf.Max(-range, -x - range);
                var y2 = Mathf.Min(range, -x + range);

                for (var y = y1; y <= y2; y++)
                {
                    var z = -x - y;
                    var position = CubeAdd(OffsetToCube(currentPosition), new Vector3(x, y, z));
                    position = CubeToOffset(position);

                    if (position == destinationPosition)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        #region Distances
        /// <summary>
        /// Distance between two hexes in cube-based coordinate system
        /// </summary>
        /// <param name="cube_1"></param>
        /// <param name="cube_2"></param>
        /// <returns></returns>
        public static int CubeDistance(Vector3 cube_1, Vector3 cube_2)
        {
            return (int)((Mathf.Abs(cube_1.x - cube_2.x) + Mathf.Abs(cube_1.y - cube_2.y) + Mathf.Abs(cube_1.z - cube_2.z)) / 2);
        }

        /// <summary>
        /// Distance between two hexes in axial-based coordinate system
        /// </summary>
        /// <param name="axial_1"></param>
        /// <param name="axial_2"></param>
        /// <returns></returns>
        public static int AxialDistance(Vector2 axial_1, Vector2 axial_2)
        {
            return CubeDistance(AxialToCube(axial_1), AxialToCube(axial_2));
        }

        /// <summary>
        /// Distance between two hexes in offset-based coordinate system
        /// </summary>
        /// <param name="offset_1"></param>
        /// <param name="offset_2"></param>
        /// <returns></returns>
        public static int OffsetDistance(Vector2 offset_1, Vector2 offset_2)
        {
            return CubeDistance(OffsetToCube(offset_1), OffsetToCube(offset_2));
        }
        #endregion
    }
}
