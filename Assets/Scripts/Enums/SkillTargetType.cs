﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SkillTargetType
{
    /// <summary>
    /// Can be used on soul placed in field directly neighbouring current position
    /// </summary>
    Direct,

    /// <summary>
    /// Can be used on soul placed in field directly neighbouring current position, but its range can reach further. Its power decline with distance.
    /// </summary>
    Piercing,

    /// <summary>
    /// Can be used from distance. Its power and accuracy decline with distance.
    /// </summary>
    Ranged,

    /// <summary>
    /// Effects affect area around selected center
    /// </summary>
    Area,

    /// <summary>
    /// Can be used only on caster
    /// </summary>
    Self
}
