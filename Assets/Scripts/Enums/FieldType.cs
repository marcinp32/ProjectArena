﻿public enum FieldType
{
    Neutral,
    Grass,
    Sand,
    Water,
    Rock
}
