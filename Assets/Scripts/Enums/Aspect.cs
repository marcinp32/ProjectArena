﻿public enum Aspect
{
    None,
    Life = 1,
    Light = 2,
    Water = 3,
    Wind = 4,
    Void = 5,
    Shadow = 6,
    Fire = 7,
    Earth = 8,
}
