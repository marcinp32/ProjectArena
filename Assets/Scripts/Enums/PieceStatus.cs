﻿public enum PieceStatus
{
    NoStatus,
    Burnt,
    Dead
}
