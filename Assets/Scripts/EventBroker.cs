﻿using System;
using System.Collections.Generic;

public class EventBroker
{
    public static event Action OnStartBattlePhase;
    public static event Action<SoulController> OnSelectActionPhase;
    public static event Action OnExecutingSkillPhase;
    public static event Action<List<string>> OnDisplayGuiEvents;
    public static event Action AfterDisplayGuiEvents;
    public static event Action<Team> OnBattleEnd;

    public static event Action<SoulController> OnCurrentSoulChange;
    public static event Action<Queue<SoulController>> OnSoulsQueueChange;
    public static event Action<SoulController> OnSoulSelected;
    public static event Action OnSoulDeselected;
    public static event Action<SoulController> OnSoulDeath;

    public static event Action<Skill> OnSkillSelected;

    public static event Action<FieldController> OnFieldHovered;
    public static event Action<FieldController> OnFieldSelected;

    public static event Action OnEndTurn;
    public static event Action<Skill, SoulController, List<FieldController>> OnExecuteSkill;
    public static event Action OnPieceChange;
    public static event Action OnUpdatePieceStats;

    public static void CallOnStartBattlePhase()
    {
        OnStartBattlePhase?.Invoke();
    }

    public static void CallOnSelectActionPhase(SoulController currentSoul)
    {
        OnSelectActionPhase?.Invoke(currentSoul);
    }

    public static void CallOnExecutingSkillPhase()
    {
        OnExecutingSkillPhase?.Invoke();
    }

    public static void CallOnDisplayGuiEvents(List<string> guiEvents)
    {
        OnDisplayGuiEvents?.Invoke(guiEvents);
    }

    public static void CallAfterDisplayGuiEvents()
    {
        AfterDisplayGuiEvents?.Invoke();
    }

    public static void CallOnBattleEnd(Team winningTeam)
    {
        OnBattleEnd?.Invoke(winningTeam);
    }

    public static void CallOnCurrentSoulChange(SoulController currentSoul)
    {
        OnCurrentSoulChange?.Invoke(currentSoul);
    }

    public static void CallOnSoulsQueueChange(Queue<SoulController> soulQueue)
    {
        OnSoulsQueueChange?.Invoke(soulQueue);
    }

    public static void CallOnSoulSelected(SoulController selectedSoul)
    {
        OnSoulSelected?.Invoke(selectedSoul);
    }

    public static void CallOnSoulDeselected()
    {
        OnSoulDeselected?.Invoke();
    }

    public static void CallOnSoulDeath(SoulController soul)
    {
        OnSoulDeath?.Invoke(soul);
    }

    public static void CallOnSkillSelected(Skill selectedSkill)
    {
        OnSkillSelected?.Invoke(selectedSkill);
    }

    public static void CallOnFieldHovered(FieldController hoveredField)
    {
        OnFieldHovered?.Invoke(hoveredField);
    }

    public static void CallOnFieldSelected(FieldController selectedField)
    {
        OnFieldSelected?.Invoke(selectedField);
    }

    public static void CallOnEndTurn()
    {
        OnEndTurn?.Invoke();
    }

    public static void CallOnExecuteSkill(Skill selectedSkill, SoulController caster, FieldController targetField)
    {
        CallOnExecuteSkill(selectedSkill, caster, new List<FieldController> { targetField });
    }

    public static void CallOnExecuteSkill(Skill selectedSkill, SoulController caster, List<FieldController> targetFields)
    {
        OnExecuteSkill?.Invoke(selectedSkill, caster, targetFields);
    }

    public static void CallOnPieceChange()
    {
        OnPieceChange?.Invoke();
    }

    public static void CallOnUpdatePieceStats()
    {
        OnUpdatePieceStats?.Invoke();
    }

}
