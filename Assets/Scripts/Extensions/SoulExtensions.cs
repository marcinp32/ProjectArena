﻿public static class SoulExtensions
{
    public static int CalculateOffensivePower(this SoulInstance soul, Skill skill)
    {
        var basePower = skill.Power + soul.OffensivePoints;

        if (soul.BaseSoul.PrimaryAspect == skill.PrimaryAspect)
        {
            basePower += (int)(basePower * 0.5F);
        }

        return basePower;
    }

    public static int CalculateDefensivePower(this SoulInstance soul, Skill skill, int damage)
    {
        var baseDamageReduction = (int)(0.5F * soul.DefensivePoints);

        if (soul.BaseSoul.PrimaryAspect == skill.PrimaryAspect)
        {
            baseDamageReduction += (int)(baseDamageReduction * 0.5F);
        }

        return baseDamageReduction;
    }
}
