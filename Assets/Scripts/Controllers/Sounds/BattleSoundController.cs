﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class Sound
{
    public string Name;
    public AudioClip Clip;
    public AudioSource Source { get; set; }
}

public class BattleSoundController : MonoBehaviour
{
    public List<Sound> Sounds;

    private void Start()
    {
        foreach(var sound in Sounds)
        {
            GameObject _go = new GameObject($"Sound{ sound.Name }");
            sound.Source = _go.AddComponent<AudioSource>();
            sound.Source.clip = sound.Clip;
        }

        EventBroker.OnCurrentSoulChange += PlayNewTurn;
        EventBroker.OnSkillSelected += PlaySkillButtonClick;
        EventBroker.OnFieldSelected += PlayFieldClick;
        EventBroker.OnExecuteSkill += PlayExecuteSkill;
        EventBroker.OnBattleEnd += PlayBattleOver;
    }

    private void PlayNewTurn(SoulController currentSoul)
    {
        Sounds.First(x => x.Name == "New Turn").Source.Play();
    }

    private void PlaySkillButtonClick(Skill skill)
    {
        Sounds.First(x => x.Name == "Skill Button Click").Source.Play();
    }

    private void PlayFieldClick(FieldController selectedField)
    {
        Sounds.First(x => x.Name == "Field Click").Source.Play();
    }

    private void PlayExecuteSkill(Skill skill, SoulController caster, List<FieldController> targets)
    {
        Sounds.First(x => x.Name == "Execute Skill").Source.Play();
    }

    private void PlayBattleOver(Team winningTeam)
    {
        Sounds.First(x => x.Name == "Battle Over").Source.Play();
    }
}
