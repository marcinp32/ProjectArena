﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class AspectColor
{
    public Aspect Aspect;
    public Color Color;
}

public class AspectColorsContainer : MonoBehaviour
{
    public List<AspectColor> AspectColors;

    public Color? GetColorByAspect(Aspect aspect)
    {
        return AspectColors.FirstOrDefault(x => x.Aspect == aspect)?.Color;
    }
}
