﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectedSoulPanelController : SoulPanelController
{
    private bool IsShowingSelected;

    // Start is called before the first frame update
    new void Start()
    {
        base.Start();

        EventBroker.OnSoulSelected += OnSoulSelected;
        EventBroker.OnSoulDeselected += OnSoulDeselected;
        EventBroker.OnFieldHovered += OnFieldHovered;
    }

    private void OnSoulSelected(SoulController selectedSoul)
    {
        IsShowingSelected = true;
        SoulSet(selectedSoul);
    }

    private void OnSoulDeselected()
    {
        IsShowingSelected = false;
        SoulReset();
    }

    private void OnFieldHovered(FieldController hoveredField)
    {
        if (!IsShowingSelected)
        {
            var objectOnField = hoveredField.ObjectOnField;

            if (objectOnField != null && objectOnField is SoulController)
            {
                SoulSet(objectOnField as SoulController);
            }
            else
            {
                SoulReset();
            }
        }
    }
}
