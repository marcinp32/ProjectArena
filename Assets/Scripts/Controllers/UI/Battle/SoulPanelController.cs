﻿using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SoulPanelController : MonoBehaviour
{
    [System.Serializable]
    public class AspectCard
    {
        public Aspect Aspect;
        public Sprite CardSprite;
    }

    public List<AspectCard> AspectCards;

    public GameObject SoulStatsPanel;

    public Animator SoulStatsPanelAnimator;
    public Animator ButtonIconAnimator;

    public TextMeshProUGUI SoulName;
    public Image SoulCard;

    private bool IsSoulStatsPanelVisible;

    protected void Start()
    {
        SoulReset();

        IsSoulStatsPanelVisible = false;
        SoulStatsPanelAnimator.SetBool("IsOpen", IsSoulStatsPanelVisible);
        ButtonIconAnimator.SetBool("IsOpen", IsSoulStatsPanelVisible);
    }

    public void OnButtonClick()
    {
        IsSoulStatsPanelVisible = !IsSoulStatsPanelVisible;
        SoulStatsPanelAnimator.SetBool("IsOpen", IsSoulStatsPanelVisible);
        ButtonIconAnimator.SetBool("IsOpen", IsSoulStatsPanelVisible);
    }

    public void SoulSet(SoulController selectedSoul)
    {
        if (SoulName != null && SoulCard != null)
        {
            SoulName.gameObject.SetActive(true);
            SoulCard.gameObject.SetActive(true);

            SoulName.text = selectedSoul.Soul.Name;
            SoulCard.sprite = AspectCards.FirstOrDefault(x => x.Aspect == selectedSoul.Soul.BaseSoul.PrimaryAspect)?.CardSprite;
        }
    }

    public void SoulReset()
    {
        SoulName?.gameObject.SetActive(false);
        SoulCard?.gameObject.SetActive(false);
    }
}
