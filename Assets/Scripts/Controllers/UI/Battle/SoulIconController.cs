﻿using UnityEngine;
using UnityEngine.UI;

public class SoulIconController : MonoBehaviour
{
    public Image SoulIcon;
    public Image AspectMarker;
    private AspectColorsContainer AspectColors;

    private void Start()
    {
        AspectColors = FindObjectOfType<AspectColorsContainer>();
    }

    public void SetSoul(SoulController soul)
    {
        SoulIcon.sprite = soul.Soul.BaseSoul.Sprite;
        var aspectColor = AspectColors.GetColorByAspect(soul.Soul.BaseSoul.PrimaryAspect);

        if (aspectColor.HasValue)
        {
            AspectMarker.color = aspectColor.Value;
        }
    }
}
