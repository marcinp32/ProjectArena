﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CurrentSoulPanelController : MonoBehaviour
{
    public SoulIconController SoulIcon;
    public Slider ApBar;
    public TextMeshProUGUI ApBarText;

    private SoulController _currentSoulValue;
    private SoulController CurrentSoul
    {
        get => _currentSoulValue;
        set
        {
            if (_currentSoulValue != null)
            {
                _currentSoulValue.ApValueChange -= OnApChange;
            }

            _currentSoulValue = value;

            if (_currentSoulValue != null)
            {
                _currentSoulValue.ApValueChange += OnApChange;
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        EventBroker.OnCurrentSoulChange += SoulSet;
    }

    private void SoulSet(SoulController currentSoul)
    {
        CurrentSoul = currentSoul;
        SoulIcon.SetSoul(currentSoul);
        OnApChange(currentSoul.CurrentActionPoints);
    }

    private void OnApChange(int currentApValue)
    {
        ApBarText.text = $"{currentApValue}/{CurrentSoul.Soul.ActionPoints} AP";
        ApBar.minValue = 0;
        ApBar.maxValue = CurrentSoul.Soul.ActionPoints;
        ApBar.value = currentApValue;
    }
}
