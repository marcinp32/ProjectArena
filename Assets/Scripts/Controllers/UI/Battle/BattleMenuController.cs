﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class BattleMenuController : MonoBehaviour
{
    public GameObject SkillsIconObject;
    public GameObject ItemsIconObject;
    public GameObject SoulsIconObject;


    public GameObject BottomBar;

    public List<GameObject> TabsContent;

    private List<BattleMenuTab> Tabs;

    private BattleMenuTab _currentTabValue;
    private BattleMenuTab CurrentTab
    {
        get => _currentTabValue;
        set
        {
            _currentTabValue = value;
            CurrentTabIndex = Tabs.IndexOf(value);
            OnTabChange(value);
        }
    }

    private int CurrentTabIndex;

    // Start is called before the first frame update
    void Start()
    {
        Tabs = new List<BattleMenuTab>
        {
            new BattleMenuTab()
            {
                TabType = BattleMenuTabType.Skills,
                IconObject = SkillsIconObject,
                TabColor = GetColorFromHex("#D61F27"),
                BottomBarText = "SKILLS"
            },

            new BattleMenuTab()
            {
                TabType = BattleMenuTabType.Items,
                IconObject = ItemsIconObject,
                TabColor = GetColorFromHex("#F48437"),
                BottomBarText = "ITEMS"
            },

            new BattleMenuTab()
            {
                TabType = BattleMenuTabType.Souls,
                IconObject = SoulsIconObject,
                TabColor = GetColorFromHex("#349645"),
                BottomBarText = "SOULS"
            }
        };

        CurrentTab = Tabs.First();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            PreviousTab();
        }
        else if (Input.GetKeyDown(KeyCode.W))
        {
            NextTab();
        }
    }

    private void PreviousTab()
    {
        CurrentTab = Tabs[CurrentTabIndex != 0 ? CurrentTabIndex - 1 : Tabs.Count - 1];
    }

    private void NextTab()
    {
        CurrentTab = Tabs[CurrentTabIndex != Tabs.Count - 1 ? CurrentTabIndex + 1 : 0];
    }

    private void OnTabChange(BattleMenuTab tab)
    {
        SetIconAnimationStatus(tab, true);
        BottomBar.GetComponentInChildren<Text>().text = tab.BottomBarText;
        BottomBar.GetComponent<Image>().color = tab.TabColor;

        ChangeTabContent(CurrentTabIndex);

        foreach(var remainingTab in Tabs.Where(x => x.TabType != tab.TabType))
        {
            SetIconAnimationStatus(remainingTab, false);
        }
    }

    private void ChangeTabContent(int index)
    {
        foreach(var tabContent in TabsContent)
        {
            tabContent.SetActive(false);
        }

        TabsContent[index].SetActive(true);
    }

    private void SetIconAnimationStatus(BattleMenuTab tab, bool isSelected)
    {
        tab.IconObject.GetComponent<Animator>().SetBool("IsSelected", isSelected);
    }

    private Color GetColorFromHex(string hexColor)
    {
        if (ColorUtility.TryParseHtmlString(hexColor, out Color result))
        {
            return result;
        }

        return new Color();
    }

    private class BattleMenuTab
    {
        public BattleMenuTabType TabType;
        public GameObject IconObject;
        public Color TabColor;
        public string BottomBarText;
    }

    private enum BattleMenuTabType
    {
        Skills,
        Items,
        Souls
    }
}
