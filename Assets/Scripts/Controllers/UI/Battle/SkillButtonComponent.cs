﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SkillButtonComponent : MonoBehaviour
{
    public Sprite OffensiveTypeSprite;
    public Sprite DefensiveTypeSprite;

    public TextMeshProUGUI NameText;
    public TextMeshProUGUI PowerText;
    public TextMeshProUGUI ApText;

    public Image SkillTypeStripe;
    public Image AspectMarker;
    public Image Container;

    public GameObject SelectionBorder;
    public GameObject DisableOverlay;

    public Material StripedBackgroundMaterial;

    private bool _isSelectedValue;
    public bool IsSelected
    {
        get => _isSelectedValue;
        set
        {
            _isSelectedValue = value;
            SelectionBorder.SetActive(value);
        }
    }

    private Image CurrentImage;

    private Skill Skill;
    private SoulController Soul;

    private Dictionary<string, Sprite> SkillTypeSprites;

    private Button Button => GetComponent<Button>();
    private AspectColorsContainer AspectColorsContainer;

    private void Start()
    {
        IsSelected = false;

        CurrentImage = GetComponent<Image>();
        AspectColorsContainer = FindObjectOfType<AspectColorsContainer>();

        SkillTypeSprites = new Dictionary<string, Sprite>()
        {
            { nameof(OffensiveSkill), OffensiveTypeSprite },
            { nameof(DefensiveSkill), DefensiveTypeSprite },
        };

        Disable();
    }

    public void SetSkill(Skill skill, SoulController soul)
    {
        if (Soul != null)
        {
            Soul.ApValueChange -= UpdateButtonValidity;
        }

        Enable();
        Skill = skill;
        Soul = soul;

        NameText.text = Skill.Name;
        PowerText.text = Skill.Power.ToString();
        ApText.text = Skill.RequiredActionPoints.ToString();

        var aspectColor = AspectColorsContainer.GetColorByAspect(skill.PrimaryAspect);

        if (aspectColor.HasValue)
        {
            AspectMarker.color = aspectColor.Value;
        }

        if (SkillTypeSprites.TryGetValue(skill.GetType().ToString(), out var skillTypeSprite))
        {
            SkillTypeStripe.sprite = skillTypeSprite;
        }

        soul.ApValueChange += UpdateButtonValidity;
        EventBroker.OnSkillSelected += OnSkillSelected;
    }

    public void Enable()
    {
        DisableOverlay.SetActive(false);
    }

    public void Disable()
    {
        DisableOverlay.SetActive(true);
    }

    public void OnClick()
    {
        IsSelected = !IsSelected;

        if (IsSelected)
        {
            EventBroker.CallOnSkillSelected(Skill);
        }
    }

    public void OnSkillSelected(Skill selectedSkill)
    {
        IsSelected = selectedSkill == Skill;
        Container.material = IsSelected ? StripedBackgroundMaterial : null;
    }

    public void UpdateButtonValidity(int remainingAp)
    {
        if (remainingAp < Skill.RequiredActionPoints)
        {
            Disable();
        }
        else
        {
            Enable();
        }
    }
}
