﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SoulsQueueController : MonoBehaviour
{
    public SoulIconController NextSoul;

    // Start is called before the first frame update
    void Start()
    {
        EventBroker.OnSoulsQueueChange += SetQueue;
    }

    private void SetQueue(Queue<SoulController> soulsQueue)
    {
        NextSoul.SetSoul(soulsQueue.Peek());
    }
}
