﻿using UnityEngine;
using UnityEngine.UI;

public class ScrollLoopController : MonoBehaviour
{
    public float ScrollSpeed;
    public Image BackgroundImage;

    private Vector2 StartPos;

    // Start is called before the first frame update
    void Start()
    {
        StartPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        BackgroundImage.material.mainTextureOffset = new Vector2(Time.time * ScrollSpeed, 0f);
    }

    //private void MoveObject()
    //{
    //    float newPosition = Time.time * (ScrollSpeed * -1);
    //    transform.position = StartPos + Vector2.right * newPosition;
    //}

    //private void CheckAndResetPositionOnRightEdge()
    //{
    //    //Debug.Log(ExtensionRect.width);
    //    //Debug.Log(Extension.transform.position.x);

    //    Vector2 rightExtensionEdge = new Vector2(Extension.transform.position.x + (ExtensionRect.width / 2), Extension.transform.position.y);
    //    var viewportPosition = Camera.main.WorldToViewportPoint(rightExtensionEdge);

    //   Debug.Log(viewportPosition);
    //    //Debug.Log(rightExtensionEdge);

    //    if (viewportPosition.x >= 1.0)
    //    {
    //        //transform.position = StartPos;
    //    }
    //}
}
