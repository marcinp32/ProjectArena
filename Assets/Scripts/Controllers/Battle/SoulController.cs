﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SoulController : FieldObjectBehaviour
{
    public event Action<int> ApValueChange;
    public event Action<int> HpValueChange;

    public SoulInstance Soul { get; set; }

    private int _currentActionPointsValue;
    public int CurrentActionPoints
    {
        get => _currentActionPointsValue;
        set
        {
            if (value >= Soul.ActionPoints)
            {
                _currentActionPointsValue = Soul.ActionPoints;
            }
            else if (value< 0)
            {
                _currentActionPointsValue = 0;
            }
            else
            {
                _currentActionPointsValue = value;
            }

            ApValueChange?.Invoke(_currentActionPointsValue);
            EventBroker.CallOnUpdatePieceStats();
        }
    }

    private int _currentHealthPointsValue;
    public int CurrentHealthPoints
    {
        get => _currentHealthPointsValue;
        set
        {
            if (value >= Soul.HealthPoints)
            {
                _currentHealthPointsValue = Soul.HealthPoints;
            }
            else if (value < 0)
            {
                _currentHealthPointsValue = 0;
            }
            else
            {
                _currentHealthPointsValue = value;
            }

            HpValueSlider.value = _currentHealthPointsValue;
            HpValueChange?.Invoke(_currentHealthPointsValue);
            EventBroker.CallOnUpdatePieceStats();

            if (_currentHealthPointsValue == 0)
            {
                CurrentField.ObjectOnField = null;
                Die();
                EventBroker.CallOnSoulDeath(this);
            }
        }
    }

    public PieceStatus CurrentStatus { get; set; }

    public TextMeshPro NameText;
    public Slider HpValueSlider;
    public SpriteRenderer SoulSprite;
    public SpriteRenderer AspectMarker;
    public SpriteRenderer TeamColorOverlay;

    [Range(0f, 1f)]
    public float TeamColorOverlayAlpha;

    public Team CurrentTeam { get; set; }

    private bool _isCurrentValue;
    public bool IsCurrent
    {
        get => _isCurrentValue;
        set
        {
            _isCurrentValue = value;
            CurrentField.CurrentSprite.SetActive(value);
        }
    }

    private AspectColorsContainer AspectColorsContainer;

    private void Start()
    {
        AspectColorsContainer = FindObjectOfType<AspectColorsContainer>();

        HpValueSlider.minValue = 0;
        HpValueSlider.maxValue = Soul.HealthPoints;
        HpValueSlider.value = Soul.HealthPoints;

        if (CurrentTeam != null)
        {
            var teamColor = CurrentTeam.Color;
            teamColor.a = TeamColorOverlayAlpha;
            TeamColorOverlay.color = teamColor;
        }

        CurrentActionPoints = Soul.ActionPoints;
        CurrentHealthPoints = Soul.HealthPoints;

        SoulSprite.sprite = Soul.BaseSoul.Sprite;

        NameText.text = Soul.Name;
        var aspectColor = AspectColorsContainer.GetColorByAspect(Soul.BaseSoul.PrimaryAspect);

        if (aspectColor.HasValue)
        {
            AspectMarker.color = aspectColor.Value;
        }
    }

    public void ExecuteSkill(Skill skill, List<FieldController> targetFields)
    {
        CurrentActionPoints -= skill.RequiredActionPoints;
        EventBroker.CallOnExecuteSkill(skill, this, targetFields);
    }

    public void TakeDamage(Skill skill, int initialDamage)
    {
        var defensivePower = Soul.CalculateDefensivePower(skill, initialDamage);
        var damage = initialDamage - defensivePower;

        CurrentHealthPoints -= damage;
    }

    protected override void OnCurrentFieldChange(FieldController previousField, FieldController currentField)
    {
        if (IsCurrent)
        {
            previousField.CurrentSprite.SetActive(false);
            currentField.CurrentSprite.SetActive(true);
        }
    }

    public override string ToString()
    {
        return $"{ Soul.Name }";
    }

    public void Die()
    {
        Destroy(gameObject);
    }
}
