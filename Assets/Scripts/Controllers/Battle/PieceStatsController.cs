﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(SoulController))]
public class PieceStatsController : MonoBehaviour
{
    public Text PieceName;
    public Slider PieceHpBar;
    public Slider PieceApBar;

    private SoulController SoulController { get; set; }
    private SoulInstance SoulInstance => SoulController.Soul;

    // Start is called before the first frame update
    void Start()
    {
        SoulController = GetComponent<SoulController>();

        EventBroker.OnUpdatePieceStats += OnUpdateStats;
        EventBroker.OnPieceChange += OnPieceChange;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnPieceChange()
    {
        if (SoulInstance != null)
        {
            PieceHpBar.minValue = 0;
            PieceHpBar.maxValue = SoulInstance.HealthPoints;

            PieceApBar.minValue = 0;
            PieceApBar.maxValue = SoulInstance.ActionPoints;
        }

        OnUpdateStats();
    }

    private void OnUpdateStats()
    {
        if (SoulInstance != null)
        {
            PieceName.text = SoulInstance.Name;
        }
        PieceHpBar.value = SoulController.CurrentHealthPoints;
        PieceApBar.value = SoulController.CurrentActionPoints;
    }
}
