﻿using System.Collections.Generic;
using System.Dynamic;

public class ExecutingSkillBattleState : BaseBattleState
{
    public ExecutingSkillBattleState(MainBattleController context) : base(context)
    {
    }

    public override void EnterState()
    {
        EventBroker.CallOnExecutingSkillPhase();
        ExecuteSkill();
        EventBroker.CallOnUpdatePieceStats();
    }

    public override void LeaveState()
    {
    }

    public void ExecuteSkill()
    {
        var skill = Context.StateDataStorage.SkillToBeExecuted;
        var skillCaster = Context.StateDataStorage.SkillCaster;
        var targetFields = Context.StateDataStorage.SkillTargetFields;

        List<string> guiEvents = new List<string>
        {
            $"{ skillCaster.Soul.Name } used { skill.Name }!"
        };

        guiEvents.AddRange(skill.ExecuteEffects(skillCaster, targetFields));
        guiEvents.Add($"{ skillCaster.Soul.Name } has { skillCaster.CurrentActionPoints } AP left.");

        Context.GuiEventController.HandleGuiEvents(guiEvents, Context.AfterDisplayGuiEvents);
    }
}
