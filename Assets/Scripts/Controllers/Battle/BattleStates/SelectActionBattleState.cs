﻿using System.Collections.Generic;

public class SelectActionBattleState : BaseBattleState
{
    public SelectActionBattleState(MainBattleController context) : base(context)
    {
    }

    public override void EnterState()
    {
        EventBroker.CallOnSelectActionPhase(Context.CurrentSoul);
    }

    public override void LeaveState()
    {

    }

    public override void CpuBehaviour()
    {
        var skills = Context.CurrentSoul.Soul.Skills;
        var selectedSkill = skills[RandomGenerator.Next(0, skills.Count)];
        var targetFields = new List<FieldController>();

        Context.CurrentSoul.ExecuteSkill(selectedSkill, targetFields);

        base.CpuBehaviour();
    }
}
