﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EndBattleState : BaseBattleState
{
    public EndBattleState(MainBattleController context) : base(context)
    {
    }

    public override void EnterState()
    {
    }

    public override void LeaveState()
    {
    }

}
