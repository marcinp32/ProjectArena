﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class StartBattleState : BaseBattleState
{
    public StartBattleState(MainBattleController context) : base(context)
    {
    }

    public override void EnterState()
    {
        EventBroker.CallOnStartBattlePhase();

        var blues = new Team()
        {
            Name = "Blues",
            Color = Color.blue,
            PlayerType = PlayerType.Player
        };

        var reds = new Team()
        {
            Name = "Reds",
            Color = Color.red,
            PlayerType = PlayerType.Player
        };

        var firstSoul = Context.SoulsContainer.InstantiateSoulByName("Charizard");
        var secondSoul = Context.SoulsContainer.InstantiateSoulByName("Blastoise");
        var thirdSoul = Context.SoulsContainer.InstantiateSoulByName("Venusaur");

        firstSoul.Skills = new List<Skill>()
        {
            Context.SoulsContainer.GetSkillByName("Boulder Mash"),
            Context.SoulsContainer.GetSkillByName("Fire Bullet"),
            Context.SoulsContainer.GetSkillByName("Wind Blade"),
            Context.SoulsContainer.GetSkillByName("Water Shot")
        };

        secondSoul.Skills = new List<Skill>()
        {
            Context.SoulsContainer.GetSkillByName("Life Beam"),
            Context.SoulsContainer.GetSkillByName("Light Ray"),
            Context.SoulsContainer.GetSkillByName("Shadow Cut"),
            Context.SoulsContainer.GetSkillByName("Void Stab")
        };

        thirdSoul.Skills = new List<Skill>()
        {
            Context.SoulsContainer.GetSkillByName("Life Beam"),
            Context.SoulsContainer.GetSkillByName("Light Ray"),
            Context.SoulsContainer.GetSkillByName("Shadow Cut"),
            Context.SoulsContainer.GetSkillByName("Void Stab")
        };

        Context.PrepareSouls(
            new List<SoulsTeam>
            {
                new SoulsTeam()
                {
                    Team = reds,
                    Souls = new List<SoulInstance>()
                    {
                        firstSoul,
                        secondSoul
                    }
                },

                new SoulsTeam()
                {
                    Team = blues,
                    Souls = new List<SoulInstance>()
                    {
                        thirdSoul
                    }
                }
            });

        var fields = Context.MapController.Fields;

        foreach(var soul in Context.AllSouls)
        {
            var randomField = fields[RandomGenerator.Next(0, fields.Count)];
            soul.CurrentField = randomField;
        }

        foreach(Aspect aspect in System.Enum.GetValues(typeof(Aspect)))
        {
            var relations = AspectRelationsHelper.GetAspectRelations(aspect);
        }

        Context.BeginNewTurn();
    }

    public override void LeaveState()
    {

    }
}
