﻿using System;

public abstract class BaseBattleState : BaseState<MainBattleController>
{
    public BaseBattleState(MainBattleController context): base(context)
    {
    }

    public virtual void CpuBehaviour()
    {
    }
}
