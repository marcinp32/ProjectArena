﻿using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(GuiEventController))]
[RequireComponent(typeof(ScriptableObjectsContainerController))]
public class MainBattleController : StateMachineBehaviour<BaseBattleState>
{
    public MapController MapController;
    public GameObject SoulPrefab;

    [HideInInspector]
    public GuiEventController GuiEventController;

    [HideInInspector]
    public ScriptableObjectsContainerController SoulsContainer;

    public List<SoulController> AllSouls { get; set; }
    public Queue<SoulController> SoulsQueue { get; set; }

    private SoulController _currentSoulValue;
    public SoulController CurrentSoul
    {
        get => _currentSoulValue;
        private set
        {
            if (_currentSoulValue != null)
            {
                _currentSoulValue.IsCurrent = false;
            }

            _currentSoulValue = value;

            if (_currentSoulValue != null)
            {
                _currentSoulValue.IsCurrent = true;
            }

            EventBroker.CallOnCurrentSoulChange(_currentSoulValue);
        }
    }

    public Text CurrentBattleStateText;

    private StartBattleState StartBattleState;
    private SelectActionBattleState SelectActionBattleState;
    private ExecutingSkillBattleState ExecutingSkillBattleState;
    private EndBattleState EndBattleState;

    protected override void OnStateChange(BaseBattleState currentState, BaseBattleState newState)
    {
        if (currentState != null)
        {
            currentState.LeaveState();
        }

        newState.EnterState();

        if (CurrentSoul != null && CurrentSoul.CurrentTeam.PlayerType == PlayerType.CPU)
        {
            newState.CpuBehaviour();
        }

        CurrentBattleStateText.text = $"Current Battle State: { CurrentState }";
    }

    public void Start()
    {
        GuiEventController = GetComponent<GuiEventController>();
        SoulsContainer = GetComponent<ScriptableObjectsContainerController>();

        StartBattleState = new StartBattleState(this);
        SelectActionBattleState = new SelectActionBattleState(this);
        ExecutingSkillBattleState = new ExecutingSkillBattleState(this);
        EndBattleState = new EndBattleState(this);

        AllSouls = new List<SoulController>();
        SoulsQueue = new Queue<SoulController>();

        EventBroker.OnExecuteSkill += ExecuteSkill;
        EventBroker.AfterDisplayGuiEvents += AfterDisplayGuiEvents;
        EventBroker.OnSoulDeath += RemoveSoul;

        CurrentState = StartBattleState;
    }

    public void PrepareSouls(List<SoulsTeam> teams)
    {
        foreach(var team in teams)
        {
            foreach (var soul in team.Souls)
            {
                var soulInstance = Instantiate(SoulPrefab);
                var soulController = soulInstance.GetComponent<SoulController>();

                soulController.Soul = soul;
                soulController.CurrentTeam = team.Team;

                AllSouls.Add(soulController);
                SoulsQueue.Enqueue(soulController);
            }
        }
    }

    public void BeginNewTurn()
    {
        var previousSoul = CurrentSoul;
        CurrentSoul = SoulsQueue.Dequeue();

        if (previousSoul != null)
        {
            SoulsQueue.Enqueue(previousSoul);
        }

        CurrentSoul.CurrentActionPoints = CurrentSoul.Soul.ActionPoints;
        CurrentState = SelectActionBattleState;

        EventBroker.CallOnSoulsQueueChange(SoulsQueue);
    }

    public void ExecuteSkill(Skill skill, SoulController caster, List<FieldController> targetFields)
    {
        StateDataStorage.SkillToBeExecuted = skill;
        StateDataStorage.SkillCaster = caster;
        StateDataStorage.SkillTargetFields = targetFields;

        CurrentState = ExecutingSkillBattleState;
    }

    public void AfterDisplayGuiEvents()
    {
        if (CurrentSoul.CurrentActionPoints <= 0)
        {
            OnTurnEndClick();
        }
        else
        {
            CurrentState = SelectActionBattleState;
        }
    }

    public void OnTurnEndClick()
    {
        BeginNewTurn();
    }

    public void OnStartAgain()
    {
        CurrentState = StartBattleState;
    }

    private void RemoveSoul(SoulController soul)
    {
        AllSouls.Remove(soul);
        var soulsQueueList = SoulsQueue.ToList();
        soulsQueueList.RemoveAll(x => x == soul);
        SoulsQueue = new Queue<SoulController>(soulsQueueList);

        CheckIfBattleEnd();
    }

    private void CheckIfBattleEnd()
    {
        if (AllSouls.Any())
        {
            var firstSoulTeam = AllSouls.First().CurrentTeam;

            if (AllSouls.All(x => x.CurrentTeam == firstSoulTeam))
            {
                EndBattle(firstSoulTeam);
            }
        }
    }

    private void EndBattle(Team winningTeam)
    {
        AllSouls.ForEach(x => x.Die());
        AllSouls.Clear();
        SoulsQueue.Clear();
        MapController.ResetMap();

        EventBroker.CallOnBattleEnd(winningTeam);
    }
}
