﻿using Assets.Scripts.Helpers;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SoulSelectedMapState : BaseMapState
{
    private bool IsSelectedFieldCurrentSoul;

    public SoulSelectedMapState(MapController context) : base(context)
    {
    }

    public override void EnterState()
    {
        EventBroker.CallOnSoulSelected(Context.SelectedFieldSoul);

        IsSelectedFieldCurrentSoul = Context.SelectedFieldSoul.IsCurrent;

        if (Context.SelectedField != null)
        {
            HandleSelectedFieldObject(Context.SelectedField);
        }
    }

    public override void LeaveState()
    {
        EventBroker.CallOnSoulDeselected();

        IsSelectedFieldCurrentSoul = false;
        Context.ClearHighlight();
    }

    public override void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            var selectedField = Context.GetClickedField();

            if (selectedField != null)
            {
                HandleFieldClick(selectedField);
            }
            else
            {
                Context.SelectedField = null;
                Context.CurrentState = Context.SelectFieldMapState;
            }
        }
    }

    private void HandleFieldClick(FieldController selectedField)
    {
        var selectedSoul = Context.SelectedFieldSoul;
        var sourceField = Context.SelectedField;
        var destinationField = selectedField;

        if (IsSelectedFieldCurrentSoul
            && HexHelper.IsInRangeOffset(sourceField.Position, destinationField.Position, selectedSoul.CurrentActionPoints)
            && selectedField.ObjectOnField == null)
        {
            var distance = HexHelper.OffsetDistance(sourceField.Position, destinationField.Position);
            selectedSoul.CurrentActionPoints -= distance;

            Context.MoveObject(Context.SelectedField, selectedField);
            Context.SelectedField = null;
        }
        else
        {
            Context.SelectedField = selectedField;
        }

        Context.CurrentState = Context.SelectFieldMapState;
    }

    private void HandleSelectedFieldObject(FieldController selectedField)
    {
        // THINK ABOUT SOMETHING BETTER
        if (selectedField.ObjectOnField != null && IsSelectedFieldCurrentSoul)
        {
            var soulController = selectedField.SoulOnField;

            if (soulController != null)
            {
                HandleSelectedSoul(selectedField, soulController);
            }
        }
    }

    private void HandleSelectedSoul(FieldController selectedField, SoulController selectedSoul)
    {
        var fields = Context.GetAllFieldsInRange(selectedField, selectedSoul.CurrentActionPoints);
        Context.ReachController.SetMovementOverlay(fields);
    }
}
