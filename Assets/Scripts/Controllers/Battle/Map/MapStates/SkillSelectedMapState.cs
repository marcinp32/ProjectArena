﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SkillSelectedMapState : BaseMapState
{
    private Skill SelectedSkill;
    private List<FieldController> SelectedSkillReach;
    private List<FieldController> SelectedSkillTarget;

    public SkillSelectedMapState(MapController context) : base(context)
    {
        SelectedSkillReach = new List<FieldController>();
        SelectedSkillTarget = new List<FieldController>();
    }

    public override void EnterState()
    {
        SelectedSkill = Context.StateDataStorage.SelectedSkill;
        SelectedSkillReach = SelectedSkill.GetFieldsInReach(Context.Fields, Context.CurrentSoul.CurrentField);
        ShowSkillReach();

        EventBroker.OnFieldHovered += ShowSkillTarget;
    }

    public override void LeaveState()
    {
        EventBroker.OnFieldHovered -= ShowSkillTarget;
        Context.ClearHighlight();
    }

    public override void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            // Contains at least one target
            if (SelectedSkillTarget.Any(x => x.SoulOnField != null))
            {
                Context.CurrentState = Context.SkillExecutionMapState;
                EventBroker.CallOnExecuteSkill(SelectedSkill, Context.CurrentSoul, SelectedSkillTarget);
            }
            // Clicked outside range
            else if (SelectedSkillTarget.Count == 0)
            {
                Context.SelectedField = Context.GetClickedField();
                Context.CurrentState = Context.SelectFieldMapState;
            }
            // No target in range
            else if(!SelectedSkillTarget.Any(x => x.SoulOnField != null))
            {
                Debug.Log("Incorrect target");
            }
        }
    }

    private void ShowSkillTarget(FieldController hoveredField)
    {
        Context.ReachController.ClearOverlay(SelectedSkillTarget);
        ShowSkillReach();
        SelectedSkillTarget = SelectedSkill.GetTargetFields(Context.Fields, SelectedSkillReach, hoveredField, Context.CurrentSoul.CurrentField);

        Context.ReachController.SetSkillTarget(SelectedSkillTarget);
    }

    private void ShowSkillReach()
    {
        Context.ReachController.SetSkillReach(SelectedSkillReach);
    }
}
