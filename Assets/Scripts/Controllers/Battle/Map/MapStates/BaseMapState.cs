﻿public abstract class BaseMapState : BaseState<MapController>
{
    public BaseMapState(MapController context) : base(context)
    {
    }

    public abstract void Update();
}
