﻿using UnityEngine;

public class SelectFieldMapState : BaseMapState
{
    public SelectFieldMapState(MapController context) : base(context)
    {
    }

    public override void EnterState()
    {
        if (Context.SelectedFieldSoul != null)
        {
            Context.CurrentState = Context.SoulSelectedMapState;
        }
    }

    public override void LeaveState()
    {
    }

    public override void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            var field = Context.GetClickedField();

            if (field != null)
            {
                Context.SelectedField = field;
                Context.SelectedField.OnClick();

                if (Context.SelectedFieldSoul != null)
                {
                    Context.CurrentState = Context.SoulSelectedMapState;
                }
            }
        }
    }
}
