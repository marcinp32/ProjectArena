﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillExecutionMapState : BaseMapState
{
    public SkillExecutionMapState(MapController context) : base(context)
    {
    }

    public override void EnterState()
    {
        EventBroker.AfterDisplayGuiEvents += AfterDisplayGuiEvents;
    }

    public override void LeaveState()
    {
        EventBroker.AfterDisplayGuiEvents -= AfterDisplayGuiEvents;
    }

    public override void Update()
    {
    }

    private void AfterDisplayGuiEvents()
    {
        Context.CurrentState = Context.SelectFieldMapState;
    }
}
