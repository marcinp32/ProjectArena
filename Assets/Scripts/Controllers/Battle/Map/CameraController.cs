﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class CameraController : MonoBehaviour
{
    public Camera Camera;
    public Tilemap Tilemap;
    public int ActiveZone;
    public float MovementTime;
    public float PanSpeed;

    [Range(0f, 1f)]
    public float ClampReduction;

    private float maxX;
    private float maxY;

    private bool IsMovingTowardsCurrentSoul;
    private Coroutine MoveTowardsCurrentSoul;

    // Start is called before the first frame update
    void Start()
    {
        var mapBounds = Tilemap.localBounds;
        maxX = mapBounds.extents.x * (1 - ClampReduction); 
        maxY = mapBounds.extents.y * (1 - ClampReduction);

        EventBroker.OnCurrentSoulChange += FocusOnCurrentSoul;
    }

    void Update()
    {
        var nextPosition = transform.position;

        // Up
        if (Input.mousePosition.y > Screen.height - ActiveZone || Input.GetKey(KeyCode.W))
        {
            nextPosition += transform.up * PanSpeed;
        }

        // Left
        if (Input.mousePosition.x < ActiveZone || Input.GetKey(KeyCode.A))
        {
            nextPosition += transform.right * -PanSpeed;
        }

        // Down
        if (Input.mousePosition.y < ActiveZone || Input.GetKey(KeyCode.S))
        {
            nextPosition += transform.up * -PanSpeed;
        }

        // Right
        if (Input.mousePosition.x > Screen.width - ActiveZone || Input.GetKey(KeyCode.D))
        {
            nextPosition += transform.right * PanSpeed;
        }

        // If changed
        if (IsMovingTowardsCurrentSoul && nextPosition != transform.position)
        {
            IsMovingTowardsCurrentSoul = false;
            StopCoroutine(MoveTowardsCurrentSoul);
        }

        transform.position = Vector3.Lerp(transform.position, nextPosition, MovementTime * Time.deltaTime);
        ClampCameraPosition();
    }

    private void ClampCameraPosition()
    {
        var cameraPosition = transform.position;
        cameraPosition.x = Mathf.Clamp(cameraPosition.x, -maxX, maxX);
        cameraPosition.y = Mathf.Clamp(cameraPosition.y, -maxY, maxY);

        transform.position = cameraPosition;
    }

    private void FocusOnCurrentSoul(SoulController currentSoul)
    {
        IsMovingTowardsCurrentSoul = true;
        MoveTowardsCurrentSoul = StartCoroutine(LerpFromTo(transform.position, currentSoul.transform.position, 0.5f));
    }

    private IEnumerator LerpFromTo(Vector2 fromPosition, Vector2 toPosition, float duration)
    {
        for (float t = 0f; t < duration; t += Time.deltaTime)
        {
            var lerpResult = Vector3.Lerp(fromPosition, toPosition, t / duration);
            lerpResult.z = transform.position.z;
            transform.position = lerpResult;
            ClampCameraPosition();

            yield return 0;
        }

        transform.position = new Vector3(toPosition.x, toPosition.y, transform.position.z);
        ClampCameraPosition();

        IsMovingTowardsCurrentSoul = false;
    }
}
