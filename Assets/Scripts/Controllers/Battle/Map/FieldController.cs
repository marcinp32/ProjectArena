﻿using Assets.Scripts.Helpers;
using UnityEngine;
using UnityEngine.Tilemaps;

public class FieldController : MonoBehaviour
{
    [HideInInspector]
    public Tile Tile;

    [HideInInspector]
    public Vector3 Position;

    public GameObject HoverSprite;
    public GameObject CurrentSprite;
    public GameObject SelectedSprite;
    public GameObject HighlightedSprite;

    private FieldObjectBehaviour _objectOnTileValue;

    /// <summary>
    /// Current object residing on this tile. SHOULD BE CHANGED DIRECTLY ONLY BY FIELDOBJECTBEHAVIOR CLASS!
    /// </summary>
    public FieldObjectBehaviour ObjectOnField
    {
        get => _objectOnTileValue;
        set
        {
            CurrentSprite.SetActive(false);
            _objectOnTileValue = value;

            if (_objectOnTileValue != null)
            {
                _objectOnTileValue.transform.position = transform.position;
                var soulController = _objectOnTileValue.GetComponent<SoulController>();
                CurrentSprite.SetActive(soulController != null && soulController.IsCurrent);
            }
        }
    }

    public SoulController SoulOnField => ObjectOnField?.GetComponent<SoulController>();

    private void Awake()
    {
        HideAllFieldSprites();
    }

    private void Update()
    {

    }

    public void SetTile(Tile tile, Vector3 position)
    {
        Tile = tile;
        Position = position;
    }

    public void HideAllFieldSprites()
    {
        HoverSprite?.SetActive(false);
        CurrentSprite?.SetActive(false);
        SelectedSprite?.SetActive(false);
        HighlightedSprite?.SetActive(false);
    }

    public void OnClick()
    {
        Debug.Log($" Offset: { Position }, Cube: { HexHelper.OffsetToCube(Position) } ");
        Debug.Log(ObjectOnField as SoulController);
    }
}
