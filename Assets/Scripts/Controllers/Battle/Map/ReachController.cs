﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class ReachController : MonoBehaviour
{
    public Color MovementOverlayColor;

    public Color SkillReachOverlayColor;
    public Color SkillReachEnemyOverlayColor;

    public Color SkillTargetOverlayColor;
    public Color SkillTargetEnemyOverlayColor;

    public Tile OverlayTile;

    private Tilemap Tilemap;

    private void Start()
    {
        Tilemap = GetComponent<Tilemap>();
    }

    public void SetSkillReach(List<FieldController> fields)
    {
        foreach (var field in fields)
        {
            var soulOnField = field.SoulOnField;
            var tileOverlay = soulOnField != null ? SkillReachEnemyOverlayColor : SkillReachOverlayColor;

            SetTileOverlay(field.Position, tileOverlay);
        }
    }

    public void SetSkillTarget(List<FieldController> fields)
    {
        foreach (var field in fields)
        {
            var soulOnField = field.SoulOnField;
            var tileOverlay = soulOnField != null ? SkillTargetEnemyOverlayColor : SkillTargetOverlayColor;

            SetTileOverlay(field.Position, tileOverlay);
        }
    }

    public void SetMovementOverlay(List<FieldController> fields)
    {
        foreach (var field in fields)
        {
            SetTileOverlay(field.Position, MovementOverlayColor);
        }
    }

    public void ClearOverlay()
    {
        for (int n = Tilemap.cellBounds.xMin; n < Tilemap.cellBounds.xMax; n++)
        {
            for (int p = Tilemap.cellBounds.yMin; p < Tilemap.cellBounds.yMax; p++)
            {
                var localPosition = new Vector3Int(n, p, (int)Tilemap.transform.position.y);

                if (Tilemap.HasTile(localPosition))
                {
                    Tilemap.SetTile(localPosition, null);
                }
            }
        }
    }

    public void ClearOverlay(List<FieldController> fields)
    {
        foreach (var field in fields)
        {
            Tilemap.SetTile(Vector3Int.CeilToInt(field.Position), null);
        }
    }

    private void SetTileOverlay(Vector3 position, Color color)
    {
        var intPosition = Vector3Int.CeilToInt(position);
        OverlayTile.color = color;
        Tilemap.SetTile(intPosition, null);
        Tilemap.SetTile(intPosition, OverlayTile);
    }
}
