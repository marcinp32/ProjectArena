﻿using UnityEngine;
using UnityEngine.Tilemaps;

public class FieldFactoryController : MonoBehaviour
{
    public GameObject FieldObjectPrefab;

    public FieldController GenerateField(Tilemap tilemap, Vector3Int localPosition)
    {
        var position = tilemap.CellToWorld(localPosition);
        var tile = tilemap.GetTile(localPosition);

        var tileGameObject = Instantiate(FieldObjectPrefab, position, Quaternion.identity);
        tileGameObject.transform.parent = tilemap.transform;
        tileGameObject.transform.position = position;

        tileGameObject.AddComponent<FieldController>();
        var fieldController = tileGameObject.GetComponent<FieldController>();
        fieldController.SetTile(tile as Tile, localPosition);

        return fieldController;
    }
}
