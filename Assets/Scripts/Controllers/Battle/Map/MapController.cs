﻿using Assets.Scripts.Helpers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Tilemaps;
using UnityEngine.UI;

[RequireComponent(typeof(FieldFactoryController))]
public class MapController : StateMachineBehaviour<BaseMapState>
{
    public Tilemap TerrainTilemap;
    public ReachController ReachController;

    public Text CurrentStateText;

    public List<FieldController> Fields;

    private FieldController _selectedFieldValue;
    public FieldController SelectedField
    {
        get => _selectedFieldValue;
        set
        {
            if (_selectedFieldValue != null)
            {
                _selectedFieldValue.SelectedSprite.SetActive(false);
            }

            _selectedFieldValue = value;

            if (_selectedFieldValue != null)
            {
                _selectedFieldValue.SelectedSprite.SetActive(true);
            }
        }
    }

    public SoulController CurrentSoul { get; set; }
    public SoulController SelectedFieldSoul => SelectedField?.SoulOnField;

    public SelectFieldMapState SelectFieldMapState;
    public SoulSelectedMapState SoulSelectedMapState;
    public SkillSelectedMapState SkillSelectedMapState;
    public SkillExecutionMapState SkillExecutionMapState;

    private FieldFactoryController FieldFactory;
    private GraphicRaycaster Raycaster;

    protected override void OnStateChange(BaseMapState currentState, BaseMapState newState)
    {
        if (currentState != null)
        {
            currentState.LeaveState();
        }

        CurrentStateText.text = $"Current state: { newState }";
        newState.EnterState();
    }

    // Start is called before the first frame update
    void Start()
    {
        FieldFactory = GetComponent<FieldFactoryController>();
        Raycaster = GetComponent<GraphicRaycaster>();

        SelectFieldMapState = new SelectFieldMapState(this);
        SoulSelectedMapState = new SoulSelectedMapState(this);
        SkillSelectedMapState = new SkillSelectedMapState(this);
        SkillExecutionMapState = new SkillExecutionMapState(this);

        CurrentState = SelectFieldMapState;
        PrepareFieldsList();

        EventBroker.OnSkillSelected += OnSkillSelected;
        EventBroker.OnCurrentSoulChange += OnCurrentSoulChange;
    }

    // Update is called once per frame
    void Update()
    {
        SetHover();
        CurrentState.Update();
    }

    public FieldController GetFieldByPosition(Vector3 position)
    {
        var tilePosition = TerrainTilemap.WorldToCell(position);

        return Fields.FirstOrDefault(x => x.Position == tilePosition);
    }

    public void MoveObject(FieldController sourceField, FieldController destinationField)
    {
        if(sourceField.ObjectOnField != null)
        {
            sourceField.ObjectOnField.CurrentField = destinationField;
        }
    }

    public void ClearHighlight()
    {
        ReachController.ClearOverlay();
    }

    public List<FieldController> GetAllFieldsInRange(FieldController center, int range)
    {
        var positions = HexHelper.GetRangeOffset(center.Position, Fields.Select(x => x.Position).ToList(), range);
        return Fields.Where(x => positions.Contains(x.Position)).ToList();
    }

    public FieldController GetClickedField()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            Vector3 clickPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            var field = GetFieldByPosition(clickPos);

            if (field != null)
            {
                EventBroker.CallOnFieldSelected(field);
            }

            return field;
        }

        return null;
    }

    public void ResetMap()
    {
        CurrentSoul.CurrentField.ObjectOnField = null;
        CurrentSoul = null;
        SelectedField = null;
    }

    private void OnSkillSelected(Skill selectedSkill)
    {
        StateDataStorage.SelectedSkill = selectedSkill;
        CurrentState = SkillSelectedMapState;
    }

    private void PrepareFieldsList()
    {
        Fields = new List<FieldController>();

        var tilemap = GetComponent<Tilemap>();
        var fieldsPositions = new List<Vector3>();

        for (int n = tilemap.cellBounds.xMin; n < tilemap.cellBounds.xMax; n++)
        {
            for (int p = tilemap.cellBounds.yMin; p < tilemap.cellBounds.yMax; p++)
            {
                var localPosition = new Vector3Int(n, p, (int)tilemap.transform.position.y);

                if (tilemap.HasTile(localPosition))
                {
                    var field = FieldFactory.GenerateField(TerrainTilemap, localPosition);
                    Fields.Add(field);
                }
            }
        }
    }

    private void OnCurrentSoulChange(SoulController currentSoul)
    {
        CurrentSoul = currentSoul;
    }

    private void SetHover()
    {
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        var tilePosition = TerrainTilemap.WorldToCell(mousePosition);

        var hoveredTile = Fields.FirstOrDefault(x => x.HoverSprite.activeSelf == true);
        var tile = Fields.FirstOrDefault(x => x.Position == tilePosition);

        if (hoveredTile != null && hoveredTile != tile)
        {
            hoveredTile.HoverSprite.SetActive(false);
        }

        if (tile != null && hoveredTile != tile)
        {
            tile.HoverSprite.SetActive(true);
            EventBroker.CallOnFieldHovered(tile);
        }
    }
}
