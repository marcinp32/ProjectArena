﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class BattleOverScreenController : MonoBehaviour
{
    public TextMeshProUGUI TeamText;

    // Start is called before the first frame update
    void Start()
    {
        HidePanel();
        EventBroker.OnBattleEnd += ShowPanel;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShowPanel(Team winningTeam)
    {
        gameObject.SetActive(true);
        TeamText.text = string.Format(TeamText.text, ColorUtility.ToHtmlStringRGB(winningTeam.Color), winningTeam.Name);
    }

    public void HidePanel()
    {
        gameObject.SetActive(false);
    }
}
