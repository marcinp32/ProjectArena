﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GuiController : MonoBehaviour
{
    public List<SkillButtonComponent> SkillButtons;

    private void Awake()
    {
        EventBroker.OnSelectActionPhase += OnSelectActionPhase;
    }

    private void OnSelectActionPhase(SoulController currentSoul)
    {
        InitSkillButtons(currentSoul);
    }

    private void InitSkillButtons(SoulController soul)
    {
        var skillsList = new List<Skill>(soul.Soul.Skills);

        foreach (var skillButton in SkillButtons)
        {
            skillButton.gameObject.SetActive(true);
            var skill = skillsList.FirstOrDefault();

            if (skill != null)
            {
                skillButton.SetSkill(skill, soul);
                skillsList.Remove(skill);
            }
            else
            {
                skillButton.Disable();
            }
        }
    }
}
