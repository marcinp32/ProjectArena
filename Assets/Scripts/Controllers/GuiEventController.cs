﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuiEventController : MonoBehaviour
{
    public GuiController GuiController;

    public void HandleGuiEvents(List<string> guiEvents, Action afterGuiEventsCallback = null)
    {
        StartCoroutine(DisplayGuiEventsCoroutine(guiEvents, afterGuiEventsCallback));
    }

    private IEnumerator DisplayGuiEventsCoroutine(List<string> guiEvents, Action afterGuiEventsCallback)
    {
        foreach (var guiEvent in guiEvents)
        {
            //GuiController.BattleStateText.text = guiEvent;
            yield return new WaitForSeconds(2f);
        }

        afterGuiEventsCallback?.Invoke();
        EventBroker.CallAfterDisplayGuiEvents();
    }
}
