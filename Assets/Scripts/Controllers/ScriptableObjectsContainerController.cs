﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ScriptableObjectsContainerController : MonoBehaviour
{
    public List<Soul> AllSouls;
    public List<Skill> AllSkills;

    public Soul GetSoulByName(string soulName)
    {
        var selectedSoul = AllSouls.First(x => x.Name == soulName);
        return selectedSoul;
    }

    public Skill GetSkillByName(string skillName)
    {
        var selectedSkill = AllSkills.First(x => x.Name == skillName);
        return selectedSkill;
    }

    public SoulInstance InstantiateSoulByName(string soulName, string nickname = null)
    {
        var soul = GetSoulByName(soulName);

        var soulInstance = ScriptableObject.CreateInstance<SoulInstance>();

        soulInstance.BaseSoul = soul;
        soulInstance.Nickname = nickname;
        soulInstance.Level = 1;
        soulInstance.HealthPoints = soul.BaseHealthPoints;
        soulInstance.ActionPoints = soul.BaseActionPoints;
        soulInstance.OffensivePoints = soul.BaseOffensivePoints;
        soulInstance.DefensivePoints = soul.BaseDefensivePoints;
        soulInstance.Speed = soul.BaseSpeed;
        soulInstance.Skills = new List<Skill>();

        return soulInstance;
    }
}
