﻿using UnityEngine;

public class Team
{
    public string Name;
    public Color Color;
    public PlayerType PlayerType;
}
