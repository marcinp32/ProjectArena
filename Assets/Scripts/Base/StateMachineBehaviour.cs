﻿using UnityEngine;

public abstract class StateMachineBehaviour<TBaseState> : MonoBehaviour
{
    public readonly dynamic StateDataStorage = new System.Dynamic.ExpandoObject();

    private TBaseState _currentStateValue;
    public TBaseState CurrentState
    {
        get => _currentStateValue;

        set
        {
            var previousState = _currentStateValue;
            _currentStateValue = value;
            OnStateChange(previousState, _currentStateValue);
        }
    }

    protected abstract void OnStateChange(TBaseState currentState, TBaseState newState);
}
