﻿using System;

public abstract class BaseState<TContext> where TContext: class
{
    protected readonly TContext Context;
    protected static Random RandomGenerator = new Random();

    public BaseState(TContext context)
    {
        Context = context;
    }

    public abstract void EnterState();
    public abstract void LeaveState();
}
