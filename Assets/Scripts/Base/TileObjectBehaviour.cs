﻿using UnityEngine;

public abstract class FieldObjectBehaviour : MonoBehaviour
{
    private FieldController _currentField;
    public FieldController CurrentField
    {
        get => _currentField;
        set
        {
            if (_currentField != null)
            {
                _currentField.ObjectOnField = null;
            }

            OnCurrentFieldChange(_currentField, value);
            _currentField = value;

            if (_currentField != null)
            {
                _currentField.ObjectOnField = this;
            }
        }
    }

    protected abstract void OnCurrentFieldChange(FieldController previousField, FieldController currentField);
}
