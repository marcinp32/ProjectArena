﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Soul", menuName = "Soul")]
public class Soul : ScriptableObject
{
    public string Name;
    public Sprite Sprite;
    public int BaseHealthPoints;
    public int BaseSpeed;
    public int BaseActionPoints;
    public int BaseOffensivePoints;
    public int BaseDefensivePoints;
    public Aspect PrimaryAspect; 
}
