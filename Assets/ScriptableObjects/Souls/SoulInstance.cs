﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoulInstance : ScriptableObject
{
    public Soul BaseSoul;

    public string Nickname;
    public string Name => string.IsNullOrEmpty(Nickname) ? BaseSoul.Name : Nickname;
    public int Level;

    public int HealthPoints;
    public int ActionPoints;
    public int OffensivePoints;
    public int DefensivePoints;
    public int Speed;

    public List<Skill> Skills;
    public PieceStatus Status;
}
