﻿using System.Collections.Generic;
using UnityEngine;

public abstract class Effect : ScriptableObject
{
    public abstract List<string> ExecuteEffectCallback(SoulController caster, SoulController target, Skill skill);
}
