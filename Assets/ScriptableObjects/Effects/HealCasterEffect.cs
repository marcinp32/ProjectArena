﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Heal Caster Effect", menuName = "Effects/Heal Caster")]
public class HealCasterEffect : Effect
{
    public int HealMultiplier;

    public override List<string> ExecuteEffectCallback(SoulController caster, SoulController target, Skill skill)
    {
        var healValue = HealMultiplier * skill.Power;
        caster.CurrentHealthPoints += healValue;

        return new List<string>
        {
            $"{ caster.Soul.Name } has healed for { healValue } HP."
        };
    }
}
