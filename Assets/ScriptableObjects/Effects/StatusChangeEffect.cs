﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Status Change Effect", menuName = "Effects/Status Change")]
public class StatusChangeEffect : Effect
{
    public PieceStatus Status;

    public override List<string> ExecuteEffectCallback(SoulController caster, SoulController target, Skill skill)
    {
        target.CurrentStatus = Status;

        return new List<string>
        {
            $"{ target.Soul.Name }'s status has changed to { Status.ToString() }."
        };
    }
}
