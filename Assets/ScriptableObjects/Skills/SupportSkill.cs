﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Support Skill", menuName = "Skills/Support Skill")]
public class SupportSkill : Skill
{
}
