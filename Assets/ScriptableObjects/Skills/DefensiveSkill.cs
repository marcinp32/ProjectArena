﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Defensive Skill", menuName = "Skills/Defensive Skill")]
public class DefensiveSkill : Skill
{
    public int Accuracy;
}
