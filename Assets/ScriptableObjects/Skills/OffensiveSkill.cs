﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName= "New Offensive Skill", menuName = "Skills/Offensive Skill")]
public class OffensiveSkill : Skill
{
    public int Accuracy;

    public override List<string> ExecuteEffects(SoulController caster, List<FieldController> targetFields)
    {
        var guiEvents = new List<string>();
        var offensivePower = caster.Soul.CalculateOffensivePower(this);
        var targets = targetFields.Where(x => x.SoulOnField != null).Select(x => x.SoulOnField);

        foreach (var target in targets)
        {
            var defensivePower = target.Soul.CalculateDefensivePower(this, offensivePower);
            var damage = offensivePower - defensivePower;

            if (damage > 0)
            {
                target.CurrentHealthPoints -= damage;
                guiEvents.Add($"{ target.Soul.Name } has taken { damage } points of damage! It has { target.CurrentHealthPoints } left.");
            }

            guiEvents.AddRange(base.ExecuteEffects(caster, targetFields));
        }

        return guiEvents;
    }
}
