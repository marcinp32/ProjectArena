﻿using Assets.Scripts.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Skill : ScriptableObject
{
    public string Name;
    public int Power;
    public int RequiredActionPoints;

    /// <summary>
    /// Range of skill around center. 0 means only selected center.
    /// </summary>
    [Tooltip("Range of skill around caster.")]
    public int Range;

    /// <summary>
    /// Range from skill center
    /// </summary>
    [Tooltip("Range of area skill around selected center.")]
    public int AreaRange;

    public SkillTargetType TargetType;
    public Aspect PrimaryAspect;
    public List<Effect> Effects;

    public virtual List<string> ExecuteEffects(SoulController caster, List<FieldController> targetFields)
    {
        var guiEvents = new List<string>();

        var allTargets = targetFields.Where(x => x.SoulOnField != null).Select(x => x.SoulOnField);

        foreach(var target in allTargets)
        {
            foreach (var effect in Effects)
            {
                guiEvents.AddRange(effect.ExecuteEffectCallback(caster, target, this));
            }
        }

        caster.CurrentActionPoints -= RequiredActionPoints;

        return guiEvents;
    }

    /// <summary>
    /// Returns fields that are in reach of this skill
    /// </summary>
    /// <param name="allFields"></param>
    /// <param name="center"></param>
    /// <returns></returns>
    public virtual List<FieldController> GetFieldsInReach(List<FieldController> allFields, FieldController center)
    {
        var centerPosition = center.Position;
        var allPositions = allFields.Select(x => x.Position).ToList();
        var selectedPositions = new List<Vector3>();

        switch (TargetType)
        {
            case SkillTargetType.Self:
                return new List<FieldController> { center };

            case SkillTargetType.Direct:
                // Direct should always have range of 1
                selectedPositions = HexHelper.GetRangeOffset(centerPosition, allPositions, 1).Where(x => x != centerPosition).ToList();
                break;

            case SkillTargetType.Piercing:
                selectedPositions = HexHelper.GetLinesOffset(center.Position, allPositions, Range).Where(x => x != centerPosition).ToList();
                break;

            case SkillTargetType.Ranged:
            case SkillTargetType.Area:
                selectedPositions = HexHelper.GetRangeOffset(centerPosition, allPositions, Range);
                break;
        }

        return allFields.Where(x => selectedPositions.Contains(x.Position)).ToList();
    }

    /// <summary>
    /// Returns fields that will be affected by this skill
    /// </summary>
    /// <param name="allFields"></param>
    /// <param name="hoveredField"></param>
    /// <param name=""></param>
    /// <returns></returns>
    public virtual List<FieldController> GetTargetFields(List<FieldController> allFields, List<FieldController> fieldsInReach, FieldController hoveredField, FieldController centerField)
    {
        if (fieldsInReach.Contains(hoveredField))
        {
            // Self can only be used on itself, no point in checking further
            if (TargetType == SkillTargetType.Self)
            {
                return fieldsInReach;
            }

            // Direct can only be used on neighbouring field
            if (TargetType == SkillTargetType.Direct || TargetType == SkillTargetType.Ranged)
            {
                return fieldsInReach.Where(x => x.Position == hoveredField.Position).ToList();
            }

            var hoveredPosition = hoveredField.Position;
            var centerPosition = centerField.Position;
            var allFieldsPositions = allFields.Select(x => x.Position).ToList();
            var fieldsInReachPositions = fieldsInReach.Select(x => x.Position).ToList();

            switch (TargetType)
            {
                case SkillTargetType.Piercing:
                    var hoveredCube = HexHelper.OffsetToCube(hoveredPosition);
                    var centerCube = HexHelper.OffsetToCube(centerPosition);
                    var fieldsInReachCube = fieldsInReachPositions.Select(x => HexHelper.OffsetToCube(x));

                    var resultPositions = new List<Vector3>();
                    var hoverSubtracted = HexHelper.CubeSubtract(centerCube, hoveredCube);

                    foreach (var fieldInReach in fieldsInReachCube)
                    {
                        var reachSubtracted = HexHelper.CubeSubtract(centerCube, fieldInReach);

                        if (Math.Sign(reachSubtracted.x) == Math.Sign(hoverSubtracted.x)
                            && Math.Sign(reachSubtracted.y) == Math.Sign(hoverSubtracted.y)
                            && Math.Sign(reachSubtracted.z) == Math.Sign(hoverSubtracted.z))
                        {
                            resultPositions.Add(HexHelper.CubeToOffset(fieldInReach));
                        }
                    }

                    return fieldsInReach.Where(x => resultPositions.Contains(x.Position)).ToList();

                case SkillTargetType.Area:
                    var positions = HexHelper.GetRangeOffset(hoveredField.Position, allFields.Select(x => x.Position).ToList(), AreaRange).ToList();
                    return allFields.Where(x => positions.Contains(x.Position)).ToList();
            }
        }

        return new List<FieldController>();
    }
}
