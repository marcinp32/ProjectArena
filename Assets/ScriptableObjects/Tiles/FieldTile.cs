﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(fileName ="New Field Tile", menuName = "Field Tile")]
public class FieldTile : TileBase
{
    public Aspect PrimaryAspect;
    public Aspect SecondaryAspect;
    public Tile Tile;
}